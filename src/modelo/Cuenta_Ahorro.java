package modelo;
public class Cuenta_Ahorro {
    private int numerodeCuenta;
    private Cliente cliente;
    private String fechadeApertura;
    private String nombreBanco;
    private float porcentajeRendimiento;
    private float saldo;
    
    public Cuenta_Ahorro(){
        
    }
    
    public Cuenta_Ahorro(int numerodeCuenta,Cliente cliente,String fechadeApertura,String nombreBanco,float porcentajeRendimiento,float saldo){
        this.numerodeCuenta = numerodeCuenta;
        this.cliente = cliente;
        this.fechadeApertura = fechadeApertura;
        this.nombreBanco = nombreBanco;
        this.porcentajeRendimiento = porcentajeRendimiento;
        this.saldo = saldo;
    }
    
    public Cuenta_Ahorro(Cuenta_Ahorro CA){
        numerodeCuenta = CA.numerodeCuenta;
        cliente = CA.cliente;
        fechadeApertura = CA.fechadeApertura;
        nombreBanco = CA.nombreBanco;
        porcentajeRendimiento = CA.porcentajeRendimiento;
        saldo = CA.saldo;
    }

    public int getNumerodeCuenta() {
        return numerodeCuenta;
    }

    public void setNumerodeCuenta(int numerodeCuenta) {
        this.numerodeCuenta = numerodeCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getFechadeApertura() {
        return fechadeApertura;
    }

    public void setFechadeApertura(String fechadeApertura) {
        this.fechadeApertura = fechadeApertura;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public float getPorcentajeRendimiento() {
        return porcentajeRendimiento;
    }

    public void setPorcentajeRendimiento(float porcentajeRendimiento) {
        this.porcentajeRendimiento = porcentajeRendimiento;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    public void Depositar(float cantidad){
        this.saldo += cantidad;
    }
    
    public boolean Retirar(float cantidad){
        if (cantidad <= saldo) {
            saldo -= cantidad;
            return true;
        } else {
            return false;
        }
    }
    public float CalcularReditos(){
        return this.porcentajeRendimiento*this.saldo/365;
    }
}