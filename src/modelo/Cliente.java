package modelo;
public class Cliente {
    private String nombreCliente;
    private String fechaNacimiento;
    private String domicilio;
    private String sexo;
    
    public Cliente(){
    
    }
    public Cliente(String nombreCliete,String fechaNacimiento,String domicilio,String sexo){
        this.nombreCliente = nombreCliete;
        this.fechaNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
        this.sexo = sexo;
    }
    
    public Cliente(Cliente C){
        nombreCliente = C.nombreCliente;
        fechaNacimiento = C.fechaNacimiento;
        domicilio = C.domicilio;
        sexo = C.sexo;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
}